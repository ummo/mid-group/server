const http = require('http')
const fs = require('fs');
const TAG = "index.js"
const express = require('express')
const hostname = "127.0.0.1"
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const util = require('util')
const user_model = require('./models/user/schema').User

const port = process.env.PORT || 3000
//Setting up default Mongoose Connection
const DB_URI = process.env.MONGODB_URI || 'mongodb://localhost:27017/ummo_mid'
mongoose.connect(DB_URI, {
    useNewUrlParser: true
})
//Getting default connection
const db = mongoose.connection
//Binding connection to error event
db.on('error', _ => {
    console.log(`${TAG}: MONGOOSE CONNECTION ATTEMPT...`, _);
    mongoose.connect()
})

db.once('open', _ => { 
    console.log(`${TAG}: MONGOOSE CONNECTION SUCCESS!`, _);
})

var app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded())

const {user_login} = require('./helpers/login')
const {user_register} = require('./helpers/register')
const {init_wallet} = require('./helpers/init-wallet')
const {update_wallet} = require('./helpers/update-wallet')

app.post('/user/login', user_login)
 
// app.put('/user/register', user_register)

app.put('/user/register', async (req, res) => {
    console.log(`${TAG}: USER REGISTERED-> ${util.inspect(req.body.contact)}`)
    
    let user = await user_model.findOne({contact: req.body.contact})
    console.log(`${TAG}: USER FOUND-> ${util.inspect(user)}`);
    
    let updatedUser = await user_model.findOneAndUpdate({contact: req.body.contact}, 
                                    {name: req.body.name, email: req.body.email, gender: req.body.gender})
                                    
    console.log(`${TAG}: UPDATED USER-> ${updatedUser}`);
    res.send(updatedUser)

}) 

//Initializing Wallet with `value` && `dateTimeStamp`
app.post('/wallet/init', init_wallet)
//Updating Wallet value
app.put('/wallet/update', update_wallet)

app.route('/user').get(function(req, res){
    res.send("USER ROUTE RESPONSE")
});

app.get('/', (req, res) => { 
    res.send('Hello World')
});

app.listen(port, () => {
    console.log(`App listening on port ${port}`)
});