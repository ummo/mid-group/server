const Wallet = require('./schema')
const TAG = "WALLET-MODEL-UPDATE"

const update = async (req, res) => {
    const wallet = req.body
    const _id = req.params._id

    try {
        _Wallet = await Wallet.updateOne(_id, {
            updated_at: Date.now(),
            ...wallet
        })

        console.log(`${TAG}: WALLET UPDATED-> ${wallet}`)

        res.status(200).send(_Wallet)
    } catch (error) {
        console.log(`${TAG}: WALLET NOT UPDATED-> ${wallet}`)
        res.status(500).send(error.toString())
    }
}

module.exports = update 