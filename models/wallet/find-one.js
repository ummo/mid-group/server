const Wallet = require('./schema')
const TAG = "WALLET-MODEL-FINDONE"

const findOne = async (req, res) => {
    const _id = req.params._id

    try {
        const _Wallet = await Wallet.findOne({_id})

        if(_Wallet) {
            res.send(_Wallet)
        } else {
            res.status(404).send('OOPS! Resource not Found!')
        }
    } catch (error) {
        res.status(500).send(error.toString())
    }
}

module.exports = findOne