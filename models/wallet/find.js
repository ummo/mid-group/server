const Wallet = require('./schema')
const TAG = "WALLET-MODEL-FIND"

const find = async (req, res) => {
    const query = req.query

    try {
        console.log(`${TAG}: WALLET FOUND`);
        const _Wallet = await Wallet.find({...query})
        res.send(_Wallet)
    } catch (error) {
        console.log(`${TAG}: WALLET NOT FOUND!`);
        res.status(500).send(error.toString())
    }
}

module.exports = find