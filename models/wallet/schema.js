var mongoose = require('mongoose')
var Schema = mongoose.Schema
var moment = require('moment')

const walletSchema = new Schema ({
    owner: String,
    points: {
        value: String,
        awarded_at: {type: Date}
        // awarded_at: Date
        // awarded_at: moment(new Date()).format('YYYY-MM-DD[T00:00:00.000Z]')
    },
    tokens: String,
    activated_at: {
        type: Date,
        default: Date.now
    },
    redeemed_at: {
        type: Date
    }
});

const Wallet = mongoose.model('wallet', walletSchema)

module.exports = {
    schema: walletSchema,
    Wallet
}