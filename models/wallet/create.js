const Wallet = require('./schema')
const TAG = "WALLET-MODEL-CREATE"

const create = async (req, res) => {
    const wallet = req.body

    try {
        _Wallet = await Wallet.create(wallet)
        res.status(201).send(_Wallet)
        console.log(`${TAG}: Wallet created -> ${_Wallet}`);
        
    } catch (error) {
        res.status(500).send(error.toString())
    }
}
 
module.exports = create