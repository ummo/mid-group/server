const User = require('./schema')
const TAG = "USER-MODEL-UPDATE"

const update = async (req, res) => {
    const user = req.body
    const _id = req.params._id

    try {
        _User = await User.updateOne(_id, {
            updated_at: Date.now(),
            ...user
        })

        console.log(`${TAG}: USER UPDATED-> ${user}`)

        res.status(200).send(_User)
    } catch (error) {
        console.log(`${TAG}: USER NOT UPDATED-> ${user}`)
        res.status(500).send(error.toString())
    }
}

module.exports = update 