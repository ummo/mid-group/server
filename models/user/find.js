const User = require('./schema')
const TAG = "USER-MODEL-FIND"

const find = async  (req, res) => {
    const query = req.query

    try {
        console.log(`${TAG}: USER FOUND`)
        const _User = await User.find({...query})
        res.send(_User)
    } catch (error) {
        console.log(`${TAG}: USER NOT FOUND!`);
        res.status(500).send(error.toString())       
    }
}

module.exports = find 