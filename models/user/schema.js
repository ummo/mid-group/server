var mongoose = require('mongoose')
var Schema = mongoose.Schema

const userSchema = new Schema ({
    name: String,
    email: String,
    contact: String,
    gender: String,
    confirmed: {
        type: Boolean,
        default: false
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    last_modified_at: {
        type: Date,
        default: Date.now
    }
});

const User = mongoose.model('user', userSchema)

module.exports = {
    schema: userSchema,
    User
}