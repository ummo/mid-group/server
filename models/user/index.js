const create = require( './create' );
const findOne = require( './find-one' );
const find = require( './find' );
const remove = require( './remove' );
const update = require( './update' );

module.exports = {
    create, findOne, find, remove, update
}