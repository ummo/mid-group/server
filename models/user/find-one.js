const User = require('./schema')
const TAG = "USER-MODEL-FINDONE"

const findOne = async (req, res) => {
    const _id = req.params._id
    
    try {
        const _User = await User.findOne({_id})

        if (_User) {
            res.send(_User)
        } else {
            res.status(404).send('OOPS! Resource Not Found!')
        }
    } catch (error) {
        res.status(500).send(error.toString())
    }
}

module.exports = findOne 