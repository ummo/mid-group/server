const User = require('./schema')
const TAG = "USER-MODEL-REMOVE"

const remove = async (req, res) => {
    const _id = req.params._id

    try {
        const deleted = await User.deleteOne(_id)
        res.send(deleted)
    } catch (error) {
        res.status(500).send(error.toString())
    }
}
 
module.exports = remove 