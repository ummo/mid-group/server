const User = require('./schema')
const TAG = "USER-MODEL-CREATE"

const create = async (req, res) => {
    const user = req.body

    try {
        _User = await User.create(user)
        res.status(201).send(_User)
        console.log(`${TAG}: User created -> ${_User}`)

    } catch (e) {
        res.status(500).send(e.toString())
    }
}

module.exports = create