const wallet_model = require('../models/wallet/schema').Wallet
const TAG = "UPDATE-WALLET.JS"

const update_wallet = async (req, res) => {
    let wallet = await wallet_model.findOneAndUpdate({owner: req.body.owner}, {points: req.body.points})

    if (wallet) {
        res.send(wallet)

    } else {
        res.status(404)
            .send({message: "Wallet NOT Found!"})
    }
} 

module.exports = {update_wallet}