const user_model = require('../models/user/schema').User
const TAG = "LOGIN.JS"

const user_login = async (req, res) => {
    let user = await user_model.findOne({contact: req.body.contact})

    if (user) {
        // const result =
        res.send(user)
    } else {        
        user = await user_model.create(req.body)
        res.send(user)
        console.log(`${TAG}: USER CREATED-> ${user}`);
    }
}

module.exports = {user_login}