const wallet_model = require('../models/wallet/schema').Wallet
const TAG = "INIT-WALLET.JS"
const util = require('util')

const init_wallet = async (req, res) => {
    let wallet = await wallet_model.findOne({owner: req.body.owner})

    console.log(`${TAG}: WALLET CREATED-> ${util.inspect(req.body.points)}`);
    if (wallet) {
        res.send(wallet)
    } else {
        wallet = await wallet_model.create(req.body)
        res.send(wallet)
        console.log(`${TAG}: WALLET CREATED-> ${wallet}`);
    }
} 

module.exports = {init_wallet}