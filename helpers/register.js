const user_model = require('../models/user/schema').User
const mongoose = require('mongoose')
const TAG = "REGISTER.JS"
// var _id = new mongoose.Types.ObjectId()

const user_register = async (req, res) => {
    let user = await user_model.findOne({contact: req.body.contact})
    console.log(`${TAG}: USER REGISTERED-> ${user}`)

    var _id = new mongoose.Types.ObjectId(user._id)

    console.log(`${TAG}: OBJECT ID-> ${_id}`)

    if (user) {
        let updatedUser = await user_model.update(_id, req.body)
        res.send(updatedUser)
        console.log(`${TAG}: USER REGISTERED-> ${updatedUser}`)
    } else {
        console.log(`${TAG}: USER NOT FOUND-> ${user}`)
    }
}

module.exports = {user_register}